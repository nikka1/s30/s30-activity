/*
	Mini activity
		Create an expressjs API designated to port 4000
		Create a new route with endpoint /hello and method GET
			- Should be able to respond with "Hello world"
*/

const express = require("express");
const mongoose = require ("mongoose");
// Mongoose is a package that allows creation of schemas to model our data structure
// Also has access to a number of methods for manipulating our database.

const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.jtm9g.mongodb.net/B157_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true

})

// Set notifications for connection success or failure
// Connection to the database
// Allows us to handle errors when the initial connection is established
let db = mongoose.connection;

// If a connection error occurred, this will be our output in the console
db.on("error", console.error.bind(console, "Connection error"));

// If the connection is successulf, this will be our output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Schemas determine the structure of the documents to be written in the database
// These acts as blueprints of our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

const memberSchema = new mongoose.Schema({
	username: String,
	password: String
})

// Model uses schemas and are used to create/instantiate objects that correspond to the schema
	// Models must be in a singular form and capitalized, because eventually it will be in plural form in MongoDB
	// The first parameter of the mongoose model method indicates the collection for storing in the MongoDB collection
	// The second parameter is used to specify the schema/blueprint of the documents
const Task = mongoose.model("Task", taskSchema);
const Member = mongoose.model("Member", memberSchema);

// Setup for allowing the server to handle data from requests
// Allows our app to read json data
app.use(express.json());

//Allows our app to read data from forms
	//urlencoded --> also a middleware
app.use(express.urlencoded({extended:true}))

// Creating a new Task
/*
	1. Add a functionality to check if there are  duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task tasks does not exist, we add it in our database
	2. The task data will be coming from the request's body
	3. Create a Task object with a "name" field/property
*/

app.post("/tasks", (req, res) =>{

	// Check if there are duplicate tasks
	// If there are no matches, the value is null
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found")
		}
		else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr) {
					return console.error(saveErr)
				}
				else {
					return res.status(201).send("New Task created.")
				}

			})
		}

	})

})

app.post("/signup", (req, res) => {

		Member.findOne({username: req.body.username}, {password: req.body.password}, (err, result) => {
			if (result != null && result.username == req.body.username) {
			return res.send ("Duplicate member found")
			}
			else {
				let newMember = new Member ({
					username: req.body.username,
					password: req.body.password
				});

				newMember.save((saveErr, savedMember) => {
					if(saveErr) {
						return console.error(saveErr)
					}
					else {
						return res.status(201).send("New Member registered")
					}
				})
			}
		})
})

// GET request to retrieve all the documents

/*
	1. Retrieve all the documents
	2. If an error is encountered, print the errir
	3. If no errors are found, send a success back to the client
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			console.log(errr)
		}
		else {
			return res.status(200).json({
				data: result
			})
		}
	})
})



app.get('/hello',(req,res)=>{
	res.send("Hello world!")

});

app.listen(port, ()=>console.log(`Server is running at port ${port}`));
